# Ada OpenAI Chat

General purpose questions to [OpenAI](https://openai.com/) models by using [Ada OpenAI](https://gitlab.com/stcarrez/ada-openai) library.

## Setup and build

Use Alire to setup and build the project:

```
git clone https://gitlab.com/stcarrez/openai-chat.git
cd openai-chat
alr build
```

## OpenAI Access Key

The [OpenAI](https://openai.com/) service uses an OAuth bearer API key
to authenticate requests made on the server.  For the credential setup you will first need to get your access key
from your [OpenAI account](https://platform.openai.com/login).
Once you have your key, create the file `openai-key.properties` file with the command:

```
echo "openai_key=XXXXX" > openai-key.properties
```

## Asking questions

After building and getting the `openai-key.properties` file setup, launch the generation tool and
give a description of the image you want to generate:

```
./bin/openai-chat write an Ada function to generate a prime number
```

You can even write your question in French or another language and get the result in your native tongue:

```
./bin/openai-chat "écrit un discours d'introduction pour ouvrir une journée de présentation au FOSDEM"
```

## API for chat

The `Create_Chat` is the main operation for the conversation chat generation.  The request is represented
by the `ChatRequest_Type` type which describes the OpenAI chat model that must be used and the
query parameters.  The request can be filled with a complete conversation chat which means it is
possible to call it several times with previous queries and responses to proceed in the chat conversation.

```
C       : OpenAI.Clients.Client_Type;
Req     : OpenAI.Models.ChatRequest_Type;
Reply   : OpenAI.Models.ChatResponse_Type;
...
   Req.Model := OpenAPI.To_UString ("gpt-3.5-turbo");
   Req.Messages.Append ((Role => Openapi.To_Ustring ("user"),
                         Content => Prompt,
                         others => <>));
   Req.Temperature := 0.3;
   Req.Max_Tokens := (Is_Null => False, Value => 1000);
   Req.Top_P := 1.0;
   Req.Frequency_Penalty := 0.0;
   Req.Presence_Penalty := 0.0;
   C.Create_Chat (Req, Reply);
```

Upon successful completion, we get a list of choices in the reply that contain the text of the conversation.
You can iterate over the list with the following code extract:

```
   for Choice of Reply.Choices loop
      Put_Line (To_WString (Choice.Message.Content));
   end loop;
```

## Links

- [OpenAI API reference - Chat](https://platform.openai.com/docs/api-reference/chat)
- [Ada OpenAI](https://gitlab.com/stcarrez/ada-openai)

